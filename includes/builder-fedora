#define _AWSCLI_FROM_PIP 1
#define _FORCE_DNF4_PKG_MANAGER 1

/* DO NOT use Dockerfile/Containerfile commands before this line */
#include "setup-from-fedora"

#include "builder-all"

#include "pipeline-fedora"

#ifdef _ELN
RUN dnf config-manager --enable fedora
#endif

RUN dnf -y install \
      aria2 \
      binutils-aarch64-linux-gnu \
      binutils-ppc64le-linux-gnu \
      binutils-s390x-linux-gnu \
      gcc-aarch64-linux-gnu \
      gcc-c++ \
      gcc-ppc64le-linux-gnu \
      gcc-s390x-linux-gnu \
      htop \
      vim-minimal \
      zeromq-devel

/* RPM building dependencies */
RUN dnf -y install \
      dwarves \
      hmaccalc \
      kabi-dw \
      libbpf-devel \
      libcap-devel \
      libcap-ng-devel \
      libtracefs-devel \
      nss-tools \
      perl-devel \
      perl-generators \
      perl-interpreter \
      python3-docutils \
      python3-sphinx \
      python3-sphinx_rtd_theme

/* New RPM building dependency */
RUN if [ "$(arch)" = "x86_64" ] ; then \
      dnf -y install \
        dracut \
        libnl3-devel \
        lvm2 \
        systemd-boot-unsigned \
        systemd-udev \
        tpm2-tools \
        ; \
    fi

/* redhat/self-test dependencies */
RUN dnf -y install bats

/* llvm and clang are needed for native tools (BPF) compilation */
RUN dnf -y install llvm clang llvm-devel

/* On top of that, lld is needed for alternative clang kernel builds. It is
 * NOT available on s390x which we currently don't care about as we cross
 * compile there.
 */
RUN if [ "$(arch)" != "s390x" ] ; then \
      dnf -y install lld ; \
    fi

#if defined _NIGHTLY_LLVM
RUN dnf -y copr enable @fedora-llvm-team/llvm-snapshots
RUN dnf -y update llvm clang llvm-devel
/* make sure these are actually pulled from Copr: header + 1 line per package */
RUN (( $(dnf repository-packages copr:copr.fedorainfracloud.org:group_fedora-llvm-team:llvm-snapshots list --installed llvm clang llvm-devel | wc -l) >= 4 ))
#endif

#ifdef _ELN
RUN dnf config-manager --disable fedora
#endif

#ifndef _ELN
/* Install an extra dependency for upstream networking selftests */
RUN if [ "$(arch)" = "x86_64" ] ; then \
      dnf -y install glibc-devel.i686 ; \
    fi
#endif

/* Recent versions of createrepo_c removed the 'createrepo' executable. :( */
RUN ln -sf /usr/bin/createrepo_c /usr/bin/createrepo

/* ARK build requires platform-python symlink */
RUN ln -sf /usr/bin/python3 /usr/libexec/platform-python

/* Extra dependencies to build selftests */
RUN dnf -y install fuse fuse-devel libmount-devel

/* BPF tooling dependencies */
RUN dnf -y install bpftool

/* Extra kernel dependencies */
RUN dnf -y install libmnl-devel libtraceevent-devel

#include "cleanup"
